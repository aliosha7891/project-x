import React from 'react'
import ReactDOM from 'react-dom'
import { ThemeProvider } from 'styled-components'
import { Provider } from 'react-redux'

import AppRouter from './router'
import theme from './styles/theme'
import store from './store'

const App = (
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <AppRouter />
    </ThemeProvider>
  </Provider>
)

ReactDOM.render(App, document.getElementById('root'))
