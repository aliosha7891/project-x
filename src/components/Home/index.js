import React, { useEffect } from 'react'

import UI from './styled'

const Home = ({
  fetchProducts,
  products
}) => {
  useEffect(() => {
    fetchProducts()
  }, [fetchProducts])

  if (products.error) return <div>{products.error}</div>
  if (products.isLoading) return <div>Loading...</div>

  return (
    <UI.Home>
      {products.payload.map(product => <div key={product._id}>{product.name}</div>)}
    </UI.Home>
  )
}

export default Home
