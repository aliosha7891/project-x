import styled from 'styled-components'

const Home = styled.div`
  color: ${({ theme }) => theme.colors.darkBlack};
`

export default {
  Home
}