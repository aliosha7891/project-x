import axios from 'axios'

const BASE_URL = 'http://192.168.0.102:3001'

export const fetchProducts = async () => await axios.get(`${BASE_URL}/products`)