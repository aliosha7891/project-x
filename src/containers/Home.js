import { connect } from 'react-redux'

import * as Products from '../actions/products'
import Home from '../components/Home'

const mapStateToProps = ({ products }) => ({
  products
})

const mapDispatchToProps = {
  ...Products
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
