import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import Home from '../containers/Home'

export default () => (
  <BrowserRouter>
    <Switch>
      <Route component={Home} path='/' exact />
      <Route />
      <Route />
      <Route />
      <Route />
    </Switch>
  </BrowserRouter>
)
