import * as constants from '../constants'

const initialState = {
  isLoading: false,
  payload: [],
  error: '',
}

export default (state = initialState, action) => {
  switch (action.type) {
    case constants.FETCH_PRODUCTS:
      return { ...state, isLoading: true }

    case constants.FETCH_PRODUCTS_SUCCESS:
      return { ...state, isLoading: false, payload: action.payload }

    case constants.FETCH_PRODUCTS_FAILED:
      return { ...state, isLoading: false, error: action.error }
  
    default:
      return state
  }
}