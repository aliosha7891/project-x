const lightTheme = {
  colors: {
    darkBlack: '#181A19',
    lightBlack: '#383639',
    beige: '#EADDDB',
    light: '#F4F4F4'
  }
};

export default lightTheme;
