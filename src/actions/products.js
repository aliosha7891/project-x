import * as constants from '../constants'
import * as api from '../api'

export const fetchProducts = () => async dispatch => {
  dispatch({
    type: constants.FETCH_PRODUCTS
  })

  try {
    const { data } = await api.fetchProducts()

    dispatch({
      type: constants.FETCH_PRODUCTS_SUCCESS,
      payload: data
    })
  } catch (err) {
    dispatch({
      type: constants.FETCH_PRODUCTS_FAILED,
      error: err.message
    })
  }
}